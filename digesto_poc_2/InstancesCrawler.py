import pandas as pd


class ComputerInstancesCrawler:

    def __init__(self, storage, cpu, memory, bandwith, price):
        self.storage = storage
        self.cpu = cpu
        self.memory = memory
        self.bandwith = bandwith
        self.price = price

    def print_console(self):
        for i, el in enumerate(self.storage):
            print(f'CPU / VCPU: {self.cpu[i]}')
            print(f'MEMORY: {self.memory[i]}')
            print(f'STORAGE / SSD DISK: {self.storage[i]}')
            print(f'BANDWIDTH / TRANSFER: {self.bandwith[i]}')
            print(f'PRICE [ $/mo ]: {self.price[i]}\n\n')

    def save_file(self, data_path='./config/data'):
        column_names = ['CPU / VCPU', 'MEMORY', 'STORAGE / SSD DISK',
                        'BANDWIDTH / TRANSFER', 'PRICE [ $/mo ]']
        computer_instances = pd.DataFrame(columns=column_names)

        for i, el in enumerate(self.storage):
            computer_instances = computer_instances.append(
                {'CPU / VCPU': self.cpu[i],
                 'MEMORY': self.memory[i],
                 'STORAGE / SSD DISK': self.storage[i],
                 'BANDWIDTH / TRANSFER': self.bandwith[i],
                 'PRICE [ $/mo ]': self.price[i],
                 },
                ignore_index=True
            )

        option_is_csv = False
        option_is_json = True

        if(option_is_csv):
            computer_instances.to_csv(data_path + '.csv')
        elif(option_is_json):
            computer_instances = computer_instances.T
            computer_instances.to_json(data_path + '.json')

        return True

    def zigzag(self, arr, size, ignore_index=None):
        ans = []
        for i in range(size):
            if ignore_index == i:
                continue
            ans.append(arr[i::size])
        return ans
