from InstancesCrawler import ComputerInstancesCrawler
from lxml import html
import requests


class DigitalOceanCrawler(ComputerInstancesCrawler):

    def __init__(self, url):
        self.url = url
        memory, cpu, bandwith, storage, price = self.extract_data()
        super().__init__(storage, cpu, memory, bandwith, price)

    def extract_data(self):
        page = requests.get(self.url)
        tree = html.fromstring(page.content)
        tr_data = tree.xpath(
            '//div[@class="slices"][1]//section[1]//table//tr/td/text()')

        return super(DigitalOceanCrawler, self).zigzag(tr_data, 6, ignore_index=5)


di = DigitalOceanCrawler(url='https://www.digitalocean.com/pricing/#droplet')
di.print_console()
