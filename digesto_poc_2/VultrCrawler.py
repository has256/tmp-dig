from InstancesCrawler import ComputerInstancesCrawler
from lxml import html
import requests


class VultrCrawler(ComputerInstancesCrawler):

    def __init__(self, url):
        self.url = url
        memory, storage, cpu, bandwith, price = self.extract_data()
        super().__init__(storage, cpu, memory, bandwith, price)

    def extract_data(self):
        page = requests.get(self.url)
        tree = html.fromstring(page.content)
        memory = tree.xpath('//div[@class="pt__cell"]/strong/text()')
        storage_cpu_bandwith_price = tree.xpath('//span/strong/text()')
        data = super(VultrCrawler, self).zigzag(
            storage_cpu_bandwith_price, 4)
        data.insert(0, memory)
        return data


vu = VultrCrawler(url='https://www.vultr.com/products/cloud-compute/#pricing')
vu.print_console()
